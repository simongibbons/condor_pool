# -*- coding: utf-8 -*-
"""
@author: koposov, sljg2
"""

from __future__ import print_function

__all__ = ["CondorPool"]


import redis
import os
import sys
import time
import cPickle
import hashlib
import collections

_PREFIX = 'condor'

_REDIS_DB = 11
_REDIS_HOSTNAME = 'cappc118.ast.cam.ac.uk'
_REDIS_PORT = 16379
_REDIS_PASS = '4671d4f329ea1be7bf646d4da918d8f4'


def _hash_function(function):
    """ Returns the md5 hash of a pickled object to enable the identification
        of it later. """
    fun_str = cPickle.dumps(_FunctionWrapper(function))
    return hashlib.md5(fun_str).hexdigest()


def _error_function(_):
    """ A default function for the pool, it should never be called. """
    raise RuntimeError("Pool was sent tasks before being told what "
                       "function to apply.")

_NoResult = collections.namedtuple("_NoResult", [])
_ClosePoolMessage = collections.namedtuple("_ClosePoolMessage", [])
_ResultWrapper = collections.namedtuple("_ResultWrapper", "argument result")

# pylint: disable=R0903
class _FunctionWrapper(object):
    """ Wraps a function to enable to to be pickled, optionally allows
    passing in a list of default args and kwargs which will be used
    for all function invocations """
    def __init__(self, function, args=None, kwargs=None):
        self.function = function
        if args is not None:
            self.args = args
        else:
            self.args = []

        if kwargs is not None:
            self.kwargs = kwargs
        else:
            self.kwargs = {}

    def __call__(self, argument):
        return self.function(argument, *self.args, **self.kwargs)

class _ArgumentWrapper(object):
    """ Wraps the arguments to a function with an id and the
    identifier of the function to use it with. We also define
    a comparison method to allow these to be used in a set """
    def __init__(self, index, argument, func_hash):
        self.index = index
        self.argument = argument
        self.func_hash = func_hash
        self.arg_hash = _hash_function(self.argument)

    def __hash__(self):
        return int(_hash_function(self.index), 16) ^\
               int(self.arg_hash, 16) ^\
               int(self.func_hash, 16)

    def __eq__(self, other):
        if isinstance(other, _ArgumentWrapper):
            return (self.index == other.index) and\
                   (self.arg_hash == other.arg_hash) and\
                   (self.func_hash == other.func_hash)
        else:
            return False
# pylint: enable=R0903

class CondorPool(object):
    """
    A pool that distributes tasks over a set of Condor processes using
    redis database as a comunicator. This pool will let you run
    emcee without shared memory, letting you use much larger machines
    with emcee.

    The pool only support the :func:`map` method at the moment because
    this is the only functionality that emcee needs. That being said,
    this pool is fairly general and it could be used for other purposes.

    Originally by Sergey Koposov
        Using some code by Joe Zuntz

    Refactoring and bug fixes by Simon Gibbons

    :param debug: (optional)
            If ``True``, print out a lot of status updates at each step.

    :param workerttl: (optional)
            Sets the time to wait before assuming that one of the worker
            threads has been killed by the scheduler. Given in seconds

            Defaults to 120s.

    :param masterwait: (optional)
            Sets the time that a worker will wait for more work when it is
            idle. If this time is exceded then it will assume that the master
            has been killed and the worker will terminate itself. This is
            to prevent zombie processes clogging up the condor queue.
            The time is given in seconds

            Defaults to 1 hours (3600 s).
    """

    def __init__(self, debug=False, workerttl=120, masterwait=3600):
        #Check if we are running in the condor environment
        try:
            rank = int(os.environ['CONDOR_RANK'])
            jobid = int(os.environ['CONDOR_JOB'])
        except:
            raise Exception("Failed to find integer valued CONDOR_RANK CONDOR_JOB variables")
        user = os.environ['CONDOR_USER']

        #Setup the class
        self.debug = debug
        self.rank = rank
        self.function = None
        self.args = None
        self.kwargs = None
        self.func_hash = None
        self.workerttl = workerttl
        self.masterwait = masterwait

        prefix = _PREFIX + '_%s_%d_' % (user, jobid)
        self.redis_keys = {
            'master'    : prefix + 'master_status',
            'argument'  : prefix + 'argument_key',
            'result'    : prefix + 'result_key',
            'function'  : prefix + 'function_key',
            'func_hash' : prefix + 'func_hash_key',
        }

        #Make the connection to the redis server
        self.con = redis.Redis(_REDIS_HOSTNAME, _REDIS_PORT,\
                               password=_REDIS_PASS, db=_REDIS_DB)

        if self.is_master():
            self.con.set(self.redis_keys['master'], 1)
            self._update_function(_error_function)
            return
        else:
            self.wait()
            sys.exit(0)

    def close(self):
        """ Close the pool, instructing all the workers to terminate """
        if self.is_master():
            for _ in xrange(1000):
                self.con.rpush(self.redis_keys['argument'], cPickle.dumps(_ClosePoolMessage()))
            time.sleep(3)
            self._cleanup()

    def _cleanup(self):
        """ Cleanup the keys we have used on the redis server """
        self.con.delete(*self.redis_keys.itervalues())

    def wait(self):
        """ The event loop for worker processes, workers will wait until
        they are told what tasks to do by the master process. """

        if self.is_master():
            raise RuntimeError("Master node told to await jobs.")

        #Event Loop
        while True:
            task_object = self.con.blpop(self.redis_keys['argument'], timeout=self.masterwait)
            if task_object == None:
                print("Worker waited for {0} seconds but recieved no tasks. Terminating".format(self.masterwait))
                sys.exit(0)

            task = cPickle.loads(task_object[1])

            if isinstance(task, _ClosePoolMessage):
                break

            if self.debug:
                print("Received Task", task.index)
                sys.stdout.flush()

            self._get_function()
            res = self.function(task.argument)

            self.con.rpush(self.redis_keys['result'], cPickle.dumps(_ResultWrapper(task, res)))



    def map(self, function, tasks, args=None, kwargs=None):
        """
        Like the built-in :func:`map` function, apply a function to all
        of the values in a list and return the list of results.

        :param function:
                The function to apply to the list.

        :param tasks:
                The list of elements.

        :param args:
                A list of additional universal positional arguments to
                the function (optional)

        :param kwargs:
               A dictionary of additional universal keyword arguments to
               the function (optional)
        """

        self._update_function(function, args, kwargs)

        wrapped_args = set([_ArgumentWrapper(index, arg, self.func_hash)
                            for index, arg in enumerate(tasks)])

        self._send_tasks(wrapped_args)

        results = [_NoResult() for _ in xrange(len(tasks))]

        while len(wrapped_args) > 0:
            try:
                result = cPickle.loads(self.con.blpop(self.redis_keys['result'], self.workerttl)[1])
            except TypeError:
                print("workerTTL reached with no new results...retrying remaining tasks\n"
                      "If this occurs often consider increasing workerttl.\n",
                      file=sys.stderr)
                sys.stderr.flush()
                self.con.delete(self.redis_keys['argument'])
                self._send_tasks(wrapped_args)
                continue

            if result.argument in wrapped_args:
                if self.debug:
                    print("Recieved Result", result.argument.index)
                    sys.stdout.flush()
                results[result.argument.index] = result.result
                wrapped_args.discard(result.argument)

        return results

    def _send_tasks(self, tasks):
        """ Appends the wrapped arguments in task_list to the worker
            processes. """
        for task in tasks:
            if self.debug:
                print("Sent Task", task.index)
                sys.stdout.flush()
            self.con.rpush(self.redis_keys['argument'], cPickle.dumps(task))

    def _get_function(self):
        """ Retrieves the current pool function """
        func_hash = self.con.get(self.redis_keys['func_hash'])
        if func_hash != self.func_hash:
            self.func_hash = func_hash
            self.function = cPickle.loads(self.con.get(self.redis_keys['function']))

    def _update_function(self, function, args=None, kwargs=None):
        """ Updates the pool function """
        if self.function != function or self.args != args or self.kwargs != kwargs:
            self.function = function
            self.args = args
            self.kwargs = kwargs
            self.func_hash = _hash_function(function)

            self.con.set(self.redis_keys['function'],
                         cPickle.dumps(_FunctionWrapper(self.function, self.args, self.kwargs)))
            self.con.set(self.redis_keys['func_hash'], self.func_hash)


    def is_master(self):
        """ Is the current process the master? """
        return 0 == self.rank

