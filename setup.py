from setuptools import setup

setup(
    name="condor_pool",
    version=0.2,
    author="Sergey Koposov and Simon Gibbons",
    py_modules=["condor_pool"],
    install_requires=["argparse", "redis", "wsgiref"]
)

